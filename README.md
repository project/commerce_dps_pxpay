# Commerce PxPay

Adds a payment gateway for Payment Express (DPS) PxPay 2.0

## Configuration

- Enable the module
- Add a new payment gateway.
- Add your credentials.
- Enjoy.
